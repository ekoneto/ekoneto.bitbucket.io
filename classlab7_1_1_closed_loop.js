var classlab7_1_1_closed_loop =
[
    [ "__init__", "classlab7_1_1_closed_loop.html#a151fa0ad6390ac9ce4b4bf18f81a1768", null ],
    [ "update", "classlab7_1_1_closed_loop.html#ada33a7cf3195d845997e4f378dbf4571", null ],
    [ "KpPos", "classlab7_1_1_closed_loop.html#aee909a93c7d06213e1cad4ed1a11826f", null ],
    [ "KpW", "classlab7_1_1_closed_loop.html#abc2fcb018cba41df8f9c77b6da138c78", null ],
    [ "Pmeas", "classlab7_1_1_closed_loop.html#ac45ac4aad21aa12e63e1e5eff5b08066", null ],
    [ "Pref", "classlab7_1_1_closed_loop.html#a4012b7eebf416c950b8891c85021b99d", null ],
    [ "Wmeas", "classlab7_1_1_closed_loop.html#adadd3f726c66f478313bb72de81a8757", null ],
    [ "Wref", "classlab7_1_1_closed_loop.html#a16c186448aedb7d61b6fe506c250db86", null ]
];