var files_dup =
[
    [ "hw0.py", "hw0_8py.html", [
      [ "TaskElevator", "classhw0_1_1_task_elevator.html", "classhw0_1_1_task_elevator" ],
      [ "Button", "classhw0_1_1_button.html", "classhw0_1_1_button" ],
      [ "MotorDriver", "classhw0_1_1_motor_driver.html", "classhw0_1_1_motor_driver" ]
    ] ],
    [ "lab1.py", "lab1_8py.html", "lab1_8py" ],
    [ "lab2.py", "lab2_8py.html", [
      [ "TaskVirtualLED", "classlab2_1_1_task_virtual_l_e_d.html", "classlab2_1_1_task_virtual_l_e_d" ],
      [ "VirtualLEDDriver", "classlab2_1_1_virtual_l_e_d_driver.html", "classlab2_1_1_virtual_l_e_d_driver" ],
      [ "TaskPhysicalLED", "classlab2_1_1_task_physical_l_e_d.html", "classlab2_1_1_task_physical_l_e_d" ],
      [ "PhysicalLEDDriver", "classlab2_1_1_physical_l_e_d_driver.html", "classlab2_1_1_physical_l_e_d_driver" ]
    ] ],
    [ "lab3_enc.py", "lab3__enc_8py.html", [
      [ "TaskEncoder", "classlab3__enc_1_1_task_encoder.html", "classlab3__enc_1_1_task_encoder" ],
      [ "EncoderDriver", "classlab3__enc_1_1_encoder_driver.html", "classlab3__enc_1_1_encoder_driver" ]
    ] ],
    [ "lab3_main.py", "lab3__main_8py.html", "lab3__main_8py" ],
    [ "lab3_shares.py", "lab3__shares_8py.html", "lab3__shares_8py" ],
    [ "lab3_user.py", "lab3__user_8py.html", [
      [ "TaskUser", "classlab3__user_1_1_task_user.html", "classlab3__user_1_1_task_user" ]
    ] ],
    [ "lab4_data.py", "lab4__data_8py.html", [
      [ "TaskData", "classlab4__data_1_1_task_data.html", "classlab4__data_1_1_task_data" ],
      [ "EncoderDriver", "classlab4__data_1_1_encoder_driver.html", "classlab4__data_1_1_encoder_driver" ]
    ] ],
    [ "lab4_main.py", "lab4__main_8py.html", "lab4__main_8py" ],
    [ "lab4_user.py", "lab4__user_8py.html", "lab4__user_8py" ],
    [ "lab5_led.py", "lab5__led_8py.html", "lab5__led_8py" ],
    [ "lab5_main.py", "lab5__main_8py.html", "lab5__main_8py" ],
    [ "lab5_shares.py", "lab5__shares_8py.html", "lab5__shares_8py" ],
    [ "lab5_user.py", "lab5__user_8py.html", "lab5__user_8py" ],
    [ "lab6.py", "lab6_8py.html", [
      [ "TaskMotor", "classlab6_1_1_task_motor.html", "classlab6_1_1_task_motor" ],
      [ "ClosedLoop", "classlab6_1_1_closed_loop.html", "classlab6_1_1_closed_loop" ],
      [ "MotorDriver", "classlab6_1_1_motor_driver.html", "classlab6_1_1_motor_driver" ],
      [ "EncoderDriver", "classlab6_1_1_encoder_driver.html", "classlab6_1_1_encoder_driver" ]
    ] ],
    [ "lab6_main.py", "lab6__main_8py.html", "lab6__main_8py" ],
    [ "lab7.py", "lab7_8py.html", [
      [ "TaskTrack", "classlab7_1_1_task_track.html", "classlab7_1_1_task_track" ],
      [ "ClosedLoop", "classlab7_1_1_closed_loop.html", "classlab7_1_1_closed_loop" ],
      [ "MotorDriver", "classlab7_1_1_motor_driver.html", "classlab7_1_1_motor_driver" ],
      [ "EncoderDriver", "classlab7_1_1_encoder_driver.html", "classlab7_1_1_encoder_driver" ]
    ] ],
    [ "lab7_main.py", "lab7__main_8py.html", "lab7__main_8py" ],
    [ "main_hw0.py", "main__hw0_8py.html", "main__hw0_8py" ],
    [ "main_lab2.py", "main__lab2_8py.html", "main__lab2_8py" ]
];