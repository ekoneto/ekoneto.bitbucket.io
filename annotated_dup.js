var annotated_dup =
[
    [ "hw0", null, [
      [ "Button", "classhw0_1_1_button.html", "classhw0_1_1_button" ],
      [ "MotorDriver", "classhw0_1_1_motor_driver.html", "classhw0_1_1_motor_driver" ],
      [ "TaskElevator", "classhw0_1_1_task_elevator.html", "classhw0_1_1_task_elevator" ]
    ] ],
    [ "lab2", null, [
      [ "PhysicalLEDDriver", "classlab2_1_1_physical_l_e_d_driver.html", "classlab2_1_1_physical_l_e_d_driver" ],
      [ "TaskPhysicalLED", "classlab2_1_1_task_physical_l_e_d.html", "classlab2_1_1_task_physical_l_e_d" ],
      [ "TaskVirtualLED", "classlab2_1_1_task_virtual_l_e_d.html", "classlab2_1_1_task_virtual_l_e_d" ],
      [ "VirtualLEDDriver", "classlab2_1_1_virtual_l_e_d_driver.html", "classlab2_1_1_virtual_l_e_d_driver" ]
    ] ],
    [ "lab3_enc", null, [
      [ "EncoderDriver", "classlab3__enc_1_1_encoder_driver.html", "classlab3__enc_1_1_encoder_driver" ],
      [ "TaskEncoder", "classlab3__enc_1_1_task_encoder.html", "classlab3__enc_1_1_task_encoder" ]
    ] ],
    [ "lab3_user", null, [
      [ "TaskUser", "classlab3__user_1_1_task_user.html", "classlab3__user_1_1_task_user" ]
    ] ],
    [ "lab4_data", null, [
      [ "EncoderDriver", "classlab4__data_1_1_encoder_driver.html", "classlab4__data_1_1_encoder_driver" ],
      [ "TaskData", "classlab4__data_1_1_task_data.html", "classlab4__data_1_1_task_data" ]
    ] ],
    [ "lab4_user", null, [
      [ "TaskUser", "classlab4__user_1_1_task_user.html", "classlab4__user_1_1_task_user" ]
    ] ],
    [ "lab5_led", null, [
      [ "LEDDriver", "classlab5__led_1_1_l_e_d_driver.html", "classlab5__led_1_1_l_e_d_driver" ],
      [ "TaskLED", "classlab5__led_1_1_task_l_e_d.html", "classlab5__led_1_1_task_l_e_d" ]
    ] ],
    [ "lab5_user", null, [
      [ "BLEDriver", "classlab5__user_1_1_b_l_e_driver.html", "classlab5__user_1_1_b_l_e_driver" ],
      [ "TaskUser", "classlab5__user_1_1_task_user.html", "classlab5__user_1_1_task_user" ]
    ] ],
    [ "lab6", null, [
      [ "ClosedLoop", "classlab6_1_1_closed_loop.html", "classlab6_1_1_closed_loop" ],
      [ "EncoderDriver", "classlab6_1_1_encoder_driver.html", "classlab6_1_1_encoder_driver" ],
      [ "MotorDriver", "classlab6_1_1_motor_driver.html", "classlab6_1_1_motor_driver" ],
      [ "TaskMotor", "classlab6_1_1_task_motor.html", "classlab6_1_1_task_motor" ]
    ] ],
    [ "lab7", null, [
      [ "ClosedLoop", "classlab7_1_1_closed_loop.html", "classlab7_1_1_closed_loop" ],
      [ "EncoderDriver", "classlab7_1_1_encoder_driver.html", "classlab7_1_1_encoder_driver" ],
      [ "MotorDriver", "classlab7_1_1_motor_driver.html", "classlab7_1_1_motor_driver" ],
      [ "TaskTrack", "classlab7_1_1_task_track.html", "classlab7_1_1_task_track" ]
    ] ]
];